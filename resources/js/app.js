require('./bootstrap');

require('alpinejs');

window.Vue = require('vue');

Vue.component('follow-button', require('./views/FollowButton.vue').default);

const app = new Vue({
    el: '#app',
});



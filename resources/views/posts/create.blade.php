@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/p" enctype="multipart/form-data" method="post">
        @csrf
        <div class="row">
            <div class="col-8 offset-2">

                <h1>Add New Post</h1>

                <div class="pt-2">
                    <x-jet-label for="caption" value="{{ __('Post Caption') }}" />
                    <x-jet-input id="caption" name="caption" class="block mt-1 w-full" type="text" caption="caption" :value="old('caption')" autofocus autocomplete="caption" />
                    @if($errors->has('caption'))
                        <strong style="color: red;">{{ $errors->first('caption') }}</strong>
                    @endif
                </div>

                <div class="pt-2">
                    <x-jet-label for="image" value="{{ __('Post Image') }}" />
                    <x-jet-input id="image" name="image" class="block mt-1 w-full" type="file" caption="image" :value="old('image')" autofocus autocomplete="image" />
                    @if($errors->has('image'))
                        <strong style="color: red;">{{ $errors->first('image') }}</strong>
                    @endif
                </div>
 
                <div class="pt-3">
                    <button class="btn btn-primary">Add New Post</button>
                </div>

            </div>
        </div> 
    </form>
</div>
@endsection
<x-app-layout>
    <!-- <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot> -->

    <!-- <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <x-jet-welcome />
            </div>
        </div>
    </div> -->

    <div class="container">
        <div class="row">
            <div class="col-3 p-5">
                <img src="{{ $user->profile->profileImage() }}" class="rounded-circle w-100">
            </div>
            <div class="col-9 pt-5">
                <div class="d-flex justify-content-between align-items-baseline">
                    <div class="d-flex align-items-center pb-3">
                        <div class="h4">{{ $user->username }}</div>
                    <follow-button user-id="{{ $user->id }}" follows="{{ $follows }}"></follow-button>
                    </div>
                
                    <!-- if login will able to see this -->
                    @can('update', $user->profile)
                        <a href="/p/create">Add New Post</a>
                    @endcan

                </div>
                
                <!-- if login will able to see this -->
                @can('update', $user->profile)
                    <a href="/dashboard/{{ $user->id }}/edit">Edit Profile</a>
                @endcan
                
                <div class="d-flex">
                    <div class="pr-5"><strong>{{ $postCount }}</strong> posts</div>
                    <div class="pr-5"><strong>{{ $followersCount }}</strong> followers</div>
                    <div class="pr-5"><strong>{{ $followingCount }}</strong> followings</div>
                </div>
                <div class="pt-4 font-weight-bold">{{ $user->profile->title }}</div>
            <div>{{ $user->profile->description }}</div>
            <div><a href="#">{{ $user->profile->url }}</a></div> <!--{{ $user->profile->url ?? 'N/A' }}-->
        </div>

        <div class="row pt-4">
            @foreach($user->posts as $post)   <!-- composer require intervention/image   ***Image manipulation library for php -->
                <div class="col-4 pb-4">
                <a href="/p/{{ $post->id }}">
                        <img src="/storage/{{ $post->image }}" alt="" class="w-100">
                    </a>
                </div>
            @endforeach

            <!-- <div class="col-4">
                <img src="https://instagram.fpnh8-1.fna.fbcdn.net/v/t51.2885-19/s320x320/106372181_271763900754742_6687690927782233677_n.jpg?_nc_ht=instagram.fpnh8-1.fna.fbcdn.net&_nc_ohc=uAwz7C3yG2EAX8QXGuS&tp=1&oh=1141c1c3809740a5769ac2095e294dde&oe=5FF6EAD1" alt="" class="w-100">
            </div>
            <div class="col-4">
                <img src="https://instagram.fpnh8-1.fna.fbcdn.net/v/t51.2885-19/s320x320/123016859_130142155532879_4489318586407338574_n.jpg?_nc_ht=instagram.fpnh8-1.fna.fbcdn.net&_nc_ohc=EglOTfc3mMwAX8BO316&tp=1&oh=4900b8e16890bafeff450fceb76c1c89&oe=5FF6DCDB" alt="" class="w-100">
            </div>
            <div class="col-4">
                <img src="https://instagram.fpnh8-1.fna.fbcdn.net/v/t51.2885-19/s320x320/125838488_369034331043369_1337214460744554814_n.jpg?_nc_ht=instagram.fpnh8-1.fna.fbcdn.net&_nc_ohc=uNji2FL_z5UAX_KynWQ&tp=1&oh=4c4724293c23b23e1a1f1b6d93412af0&oe=5FF7F764" alt="" class="w-100">
            </div> -->
        </div>
    </div>

</x-app-layout>

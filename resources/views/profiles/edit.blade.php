@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/dashboard/{{ $user->id }}" enctype="multipart/form-data" method="post">
        @csrf
        @method('PATCH')

        <div class="row">
            <div class="col-8 offset-2">

                <h1>Edit Profile</h1>

                <div class="pt-2">  
                    <x-jet-label for="title" value="{{ __('Title') }}" />                                             <!-- old mean when fail validation it comeback with the same value -->
                    <x-jet-input id="title" name="title" class="block mt-1 w-full" type="text" caption="title" :value="old('title') ?? $user->profile->title " autofocus autocomplete="title" />
                    @if($errors->has('title'))
                        <strong style="color: red;">{{ $errors->first('title') }}</strong>
                    @endif
                </div>

                <div class="pt-2">
                    <x-jet-label for="description" value="{{ __('Description') }}" />
                    <x-jet-input id="description" name="description" class="block mt-1 w-full" type="text" caption="description" :value="old('description') ?? $user->profile->description " autofocus autocomplete="description" />
                    @if($errors->has('description'))
                        <strong style="color: red;">{{ $errors->first('description') }}</strong>
                    @endif
                </div>

                <div class="pt-2">
                    <x-jet-label for="url" value="{{ __('Url') }}" />
                    <x-jet-input id="url" name="url" class="block mt-1 w-full" type="text" caption="url" :value="old('url') ?? $user->profile->url" autofocus autocomplete="url" />
                    @if($errors->has('url'))
                        <strong style="color: red;">{{ $errors->first('url') }}</strong>
                    @endif
                </div>

                <div class="pt-2">
                    <x-jet-label for="image" value="{{ __('Profile Image') }}" />
                    <x-jet-input id="image" name="image" class="block mt-1 w-full" type="file" caption="image" :value="old('image')" autofocus autocomplete="image" />
                    @if($errors->has('image'))
                        <strong style="color: red;">{{ $errors->first('image') }}</strong>
                    @endif
                </div>
 
                <div class="pt-3">
                    <button class="btn btn-primary">Update Profile</button>
                </div>

            </div>
        </div> 
    </form>
</div>
@endsection
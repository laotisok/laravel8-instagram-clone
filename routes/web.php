<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfilesController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\FollowsController;
use App\Mail\NewUserWelcomeMail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Routes need to stay in oder


//Route to show specific profile of user
Route::get('/dashboard/{user}', [ProfilesController::class, 'index'])->name('dashboard.show');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    $user = Auth::user();
    return Redirect::to('/dashboard/'.$user->id);
})->name('dashboard');

//Instagram hompage
Route::get('/homepage', [PostsController::class, 'index']);

Route::get('/email', function() {
    return new NewUserWelcomeMail();
});

Route::get('/', function () {
    return view('welcome');
});

Route::post('follow/{post}', [FollowsController::class, 'store']);

//Route to create Post
Route::get('/p/create', [PostsController::class, 'create']);

//Route to save Post
Route::post('/p', [PostsController::class, 'store']);

//Rout to view Post
ROute::get('/p/{post}', [PostsController::class, 'show']);

//Route to update
Route::patch('/dashboard/{user}', [ProfilesController::class, 'update'])->name('profile.update');

//Rout to view edit page
Route::get('/dashboard/{user}/edit', [ProfilesController::class, 'edit'])->name('dashboard.edit');
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //use HasFactory;

    protected $guarded = []; //disable security
    
    public function profileImage(){
        $imagePath = ($this->image) ? $this->image : 'profile/no_image.png';;
        return '/storage/'.$imagePath;
    }

    public function followers(){
        return $this->belongsToMany(User::class);
    }

    public function user(){  //Profile fetch user
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{

    //Add this so all the function below will need authorication
    public function __construct(){
        $this->middleware('auth');
    }

    public function index() {
        $users = auth()->user()->following()->pluck('profiles.user_id');  //get all the following user_id

        //create pagination   //with mean load with user, do so make relationship with function user in Post Model. so query it run only one time and no limit function in query.
        $posts = Post::whereIn('user_id', $users)->with('user')->latest()->paginate(2);  //fetch all the post  //latest() = orderBy('created_at', 'DESC')
        //dd($posts);
        return view('posts.index', compact('posts'));
    }

    public function create(){
        return view('posts.create');
    }

    public function store(){
        //give the request, validate it and give back the the data to $data.
        $data = request()->validate([
            //'another' => '',  //mean other field no need validation, do this when u're not sure have many request 
            'caption' => 'required',
            'image' => ['required','image'],  //'required|image'
        ]);


        //Style save in tinker
        //$post = new \App\Post();
        //$post->caption = $data['camption'];
        //$post->save();

        //Post::truncate();  //delete data from DB

        //php artisan storage:link   ***run this to make storage public, so we will able to run picture public.
        //after run cmd above we will able to see img with this link: http://127.0.0.1:8000/storage/uploads/YtGWAUsSxSx1UxiilTPJezkxwhFdODWooTFjPAxr.jpg
        $imagePath = request('image')->store('uploads', 'public');

        //make image fit 1200w*1200h
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(1200, 1200);
        $image->save();

        auth()->user()->posts()->create([
            'caption' => $data['caption'],
            'image' => $imagePath
        ]);

        return redirect('/dashboard/'.auth()->user()->id);

        //\App\Post::create($data);

        //dd(request()->all());
    }

    //\App\Models\Post that make laravel fetch our post automaticly (so this $post have all the in formation not just ID)
    //This also fetch findOrFail automaticly
    public function show(\App\Models\Post $post){  
        
        // dd($post->image);
        return view('posts.show', compact('post'));  //compact is same thing as variable $post, it match post to $post
        
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class FollowsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function store($user){
        $user = User::findOrFail($user);
        // return $user->username;
        return auth()->user()->following()->toggle($user->profile); //use auth user to connect to other profile
    }
}

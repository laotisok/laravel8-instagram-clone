<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;


class ProfilesController extends Controller
{
    public function index($user){
        $user = User::findOrFail($user);  //find($user);
        
        //mean if auth user following that user (id of following user in auth user) will return true
        $follows = (auth()->user())? auth()->user()->following->contains($user->id) : false;

        //this will make laravel run faster not just alway hit database every single time.
        // $postCount = Cache::remember(
        //     'count.posts.'. $user->id, //count post with this user id
        //     now()->addSeconds(30), //store data time
        //     function() use ($user){  //if it not there this will run
        //         return $user->posts->count();
        //     }
        // );

        // $followersCount = Cache::remember(
        //     'count.followers.'. $user->id, 
        //     now()->addSeconds(30), //store data time
        //     function() use ($user){  //if it not there this will run
        //         return  $user->profile->followers->count();
        //     }
        // );

        // $followingCount = Cache::remember(
        //     'count.following.'. $user->id, 
        //     now()->addSeconds(30), //store data time
        //     function() use ($user){  //if it not there this will run
        //         return  $user->following->count();
        //     }
        // );

        $postCount = $user->posts->count();
        $followersCount = $user->profile->followers->count();
        $followingCount = $user->following->count();

        // return view('profiles.index', ['user' => $user]);
        return view('profiles.index', compact('user', 'follows', 'postCount', 'followersCount', 'followingCount'));
    } 

    public function edit(\App\Models\User $user){
        $this->authorize('update', $user->profile); //cannot access to this page unless u authorize
        return view('profiles.edit', compact('user'));
    }

    public function update(User $user){
        $this->authorize('update', $user->profile); //cannot access to this page unless u authorize
        $data = request()->validate([
            'title' => 'required',
            'description'=>'required',
            'url'=>'url',
            'image'=>'image'
        ]);
        
        //dd($data);
        if(request('image')){
            $imagePath = request('image')->store('profile', 'public');

            //make image fit 1200w*1200h
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1000, 1000);
            $image->save();

            $imageArray = ['image'=>$imagePath];  //this wiil override image in $data, and set only imagePath
        }

        

        //auth()->user() mean get only authentication user
        //auth()->user()->profile->update($data);
        auth()->user()->profile->update(array_merge(
            $data, //$data have field image but complex
            $imageArray ?? []  //$imageArray not set it set to []
        ));

        return redirect('/dashboard/'.$user->id);
    }
}
